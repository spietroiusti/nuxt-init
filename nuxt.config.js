// import axios from "axios";


module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
//  https://medium.com/a-man-with-no-server/static-site-generators-nuxt-js-2fa9782d27c8
  generate: {
    routes: [
      '/',
      '/users/1',
      '/users/2'
    ]
    // routes: function () {
    //   return axios.get('https://jsonplaceholder.typicode.com/users')
    //     .then((res) => {
    //       return res.data.map((user) => {
    //         return {
    //           route: '/users/' + user.id,
    //           payload: user
    //         }
    //       })
    //     })
    // }
  },
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/bootstrap-vue',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  // https://github.com/nuxt/nuxt.js/issues/5049
  // bootstrapVue: {
  // bootstrapCSS: false, // here you can disable automatic bootstrapCSS in case you are loading it yourself using sass
  // bootstrapVueCSS: false // CSS that is specific to bootstrapVue components can also be disabled. That way you won't load css for modules that you don't use
  // componentPlugins: [], // Here you can specify which components you want to load and use
  // directivePlugins: [] // Here you can specify which directives you want to load and use. Look into official docs to get a list of what's available
  // },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      if (ctx.isClient) {
        // BootstrapVue and PortalVue require access to the global Vue reference (via import Vue from 'vue').
        // config.resolve.alias['vue$'] = 'vue/dist/vue.esm.js'
      }
    }
  }
}
